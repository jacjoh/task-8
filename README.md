# task-8

Console application

- creates multiple animal objects and stores them in a collection. There are two different animals in the zoo: Bird and Dogs.
- Animals all have a name, weight, and a bool for checking if it can fly
- Both dogs and birds inherit from animal
- The animal class is abstract and has an abstract void function for talking, this function is implemented in the different classes.
- There is also a dog accessory class that the dog class uses.
- There are created interfaces for running and flying. Dogs run, and birds try to fly.
