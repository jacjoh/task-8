﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task_8_zoology
{
    public interface IFlyer<T>
    {
        public void Fly();
    }
    
    public interface IRunner<T>
    {
        public void Run();
    }
}
